import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import table from './component/table'
import * as serviceWorker from './serviceWorker';
// import Cup from './component/cup';
// import Tables from './component/table'
// import { BrowserRouter, Route, Switch } from "react-router-dom";


ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<table />, document.getElementById('header'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
