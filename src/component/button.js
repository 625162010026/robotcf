import React from 'react';
// import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from 'react-bootstrap/Button';
//import MaterialTableDemo from './main';
//import { makeStyles } from '@material-ui/core/styles';
import Table from 'react-bootstrap/Table';
import './main.css';
import Select from "react-dropdown-select";
import $ from 'jquery';
import Draggable from 'react-draggable';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

// import Split from 'react-split'
const maxtable = 10;
const num_table = [];
var table_x_y = ['(0,0)', '(1,1)', '(1,2)', '(1,3)', '(2,1)', '(2,2)', '(2,3)', '(3,1)', '(3,2)', '(3,3)'];
var sum_have = 0;
var slot_have = [];
var table_position = 0;
var table_num = 0;
var table_row = 0;


class Toggle extends React.Component {
    constructor(props) {
        super(props);


    }

    get_num_table(el) { // Show id table and position of table 
        // console.log( el.target.className );
        var split = el.target.className.split(" ")[0];
        // console.log(split);
        var num_table = split.split("tables")[1];
        console.log(num_table);
        console.log(table_x_y[num_table]);
        table_num = num_table;
        table_position = table_x_y[num_table]
        alert("ADD Table")

    }

    change_cup(img) { //Change img cup have or not in slot
        var sets = (img.target.src).split("_");
        if (sets[1] == "dont.png") {
            img.target.src = "cups_have.png";
        }
        else {
            img.target.src = "cups_dont.png";
        }
    }

    check_cups_have(img) { //Add cup in array list and count num of cups

        var check_src = img.target.src;

        console.log(check_src);
        var sets = check_src.split("_");
        console.log(sets);
        if (sets[1] == 'have.png') {

            if ((slot_have.find(element => element == img.target.className)) != img.target.className) { //check add cup repeat in list ?
                slot_have.push(img.target.className);
                sum_have += 1;
                console.log("add cups");


            }
        }
        else if (sets[1] == 'dont.png') {
            if ((slot_have.find(element => element == img.target.className)) == img.target.className) { // check data in list ?
                var index = slot_have.indexOf(img.target.className);
                slot_have.splice(index, 1);
                sum_have -= 1;
                console.log("del cups");

            }
        }
        console.log(sum_have);
        console.log(slot_have);
    }

    createTable = () => { // create table and postion array 2d

        for (let i = 1; i <= maxtable; i++) {
            num_table.push({ label: i, value: table_x_y[i] })

        }
    }

    onClick_img(ev) {//check change cup and check cup have in list
        this.change_cup(ev)
        this.check_cups_have(ev);
    }

    accept_createtable() { //accept createtable in array list
        console.log("____________________________");
        slot_have.sort();
        console.log(sum_have);
        console.log(slot_have);
        var table_last = 0;
        if (table_num != 0) {
            for (var loop_table = 0; loop_table < slot_have.length; loop_table++) { //loop in data  array_list
                //     9 == 9 
                if (table_num != table_last) { // เช็คว่าสร้างโต๊ะมาจริงไหม ถ้าจริงทำการสร้างตารางขึ้นมา
                    document.getElementById('list_menu_history').innerHTML += "<tr id='row" + table_row + "'></tr>";
                    document.getElementById("row" + table_row).innerHTML = "<th id='id" + table_row + "'>" + (table_row + 1) + "</th>"
                        + "<th id='table" + table_row + "'>" + table_num + "</th>"
                        + "<th id='slot" + table_row + "'>" + slot_have[loop_table].split("img")[1] + "</th>";
                    table_last = table_num;

                }
                else {
                    document.getElementById("slot" + table_row).innerHTML += " , " + slot_have[loop_table].split("img")[1] + ""; //list slot in table
                }
            }
            table_row++;
        }
        else {
            alert("Please Choose Table First");
        }


    }

    clear_list_table(el) {
        document.getElementById('list_menu_history').innerHTML = '<tr>'
            + '<th>id</th>'
            + '<th>Table</th>'
            + '<th>Slot</th>'
            + '</tr>';
        table_num = 0;
        table_row = 0;
        sum_have = 0;
        slot_have = [];
        console.log(slot_have);
        for (var num_pic = 1; num_pic <= 6; num_pic++) {
            document.getElementById('img' + num_pic).src = "cups_dont.png";
        }
    }

    sendMenu_to_robot(el) {

        var array_obj = {};
        var start_id = 0;
        if (table_row != 0) {
            if (table_row != 0) {
                for (start_id = 0; start_id < table_row; start_id++) {
                    var data1 = document.getElementById('id' + start_id).innerHTML;
                    var data2 = document.getElementById('table' + start_id).innerHTML;
                    var data3 = document.getElementById('slot' + start_id).innerHTML;
                    console.log("id = " + data1 + " | table=" + data2 + " | slot=" + data3);
                    var array_list = {};
                    array_list["id"] = data1;
                    array_list["table"] = data2;
                    array_list["slot"] = data3;
                    console.log("id = " + data1 + " | table=" + data2 + " | slot=" + data3);
                    array_obj[(start_id + 1)] = array_list;
                }
            }
            alert("Let' Go!!")
            console.log(array_obj);
        }
        else {
            alert("Please Add List Cups Menu First")
        }
    }

    eventLogger = (e: MouseEvent, data: Object) => {
        console.log('Event: ', e);
        console.log('Data: ', data);
    };


    render() {
        return (
            <div>


<Container>
 
 <Row>

 
 



                {/* <div className="table"  >

                    <Table striped bordered hover class="table table-dark">
                        <thead id="list_menu_history">
                            <tr>
                                <th>id</th>
                                <th>Table</th>
                                <th>Slot</th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </Table>
                </div> */}
               
            <Col lg={true} className ="col1">
            <h2 className= "cupfront">---- Menu List ----</h2>
                <div className="table" class="tablemain">
                
                    <Table striped bordered hover class="table table-dark" >
                        <thead id="list_menu_history">
                            <tr>

                                <th class="table-title">id</th>
                                <th class="table-title">Table</th>
                                <th class="table-title">Slot</th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </Table>
                </div>
                </Col>
                <Col sm={8} className = "col2">
                

                <div className="img">
                    <img className="img1" id="img1" src="cups_dont.png" alt="Smiley face" height="150" width="120" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img2" id="img2" src="cups_dont.png" alt="Smiley face" height="150" width="120" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img3" id="img3" src="cups_dont.png" alt="Smiley face" height="150" width="120" onClick={ev => this.onClick_img(ev)}></img>
                    <br />
                    <img className="img4" id="img4" src="cups_dont.png" alt="Smiley face" height="150" width="120" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img5" id="img5" src="cups_dont.png" alt="Smiley face" height="150" width="120" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img6" id="img6" src="cups_dont.png" alt="Smiley face" height="150" width="120" onClick={ev => this.onClick_img(ev)}></img>

                </div>

                <div className="button" >



                 






                    {/* <Draggable
                        defaultClassNameDragging="move1"
                        axis="x"
                        handle=".table4"
                        defaultPosition={{ x: 60, y: 104 }}
                        position={null}
                        grid={[25, 25]}
                        scale={1}

                        onStart={this.handleStart}
                        onDrag={this.handleDrag}
                        onStop={this.handleStop}>
                        <div>

                            <img className="table4 move" src="exchange-arrows.png" width="30" height="30" ></img>

                            <div>
                                <Button className=" tables4  table-value" id="tables4" onClick={el => this.get_num_table(el)}
                                >Table 4</Button>
                            </div>

                        </div>

                    </Draggable>
                    
              */}
              <br />
                   {/* <h1>Table</h1> */}
                   <h2 className= "Tables">----------- Choose Table -----------</h2>
                    <Button className="tables1  table-value " id="tables1" onClick={el => this.get_num_table(el)}>Table1</Button>
                    <Button className="tables2  table-value" id="tables2" onClick={el => this.get_num_table(el)}>Table2</Button>
                    <Button className="tables3  table-value" id="tables3" onClick={el => this.get_num_table(el)}>Table3</Button>
                    <Button className="tables4  table-value" id="tables4" onClick={el => this.get_num_table(el)}>Table4</Button>
                    <Button className="tables5  table-value" id="tables5" onClick={el => this.get_num_table(el)} onClick={el => this.get_num_table(el)}>Table 5</Button>

                    <Button className="tables6  table-value" id="tables6" onClick={el => this.get_num_table(el)}>Table6</Button>
                    <Button className="tables7  table-value" id="tables7" onClick={el => this.get_num_table(el)}>Table7</Button>
                    <br />


                    <Button className="tables8 table-value" id="tables8" onClick={el => this.get_num_table(el)}>Table8</Button>
                    <Button className="tables9 table-value" id="tables9" onClick={el => this.get_num_table(el)}>Table9</Button>
                    <Button className="tables10 table-value" id="tables10 " onClick={el => this.get_num_table(el)}>Table10</Button>
                    <Button className="tables11 table-value" id="tables11 " onClick={el => this.get_num_table(el)}>Table11</Button>
                    <Button className="tables12 table-value" id="tables12 " onClick={el => this.get_num_table(el)}>Table12</Button>
                    <Button className="tables13 table-value" id="tables13 " onClick={el => this.get_num_table(el)}>Table13</Button>
                    <Button className="tables14 table-value" id="tables14 " onClick={el => this.get_num_table(el)}>Table14</Button>
                    <Button className="tables15 table-value" id="tables15 " onClick={el => this.get_num_table(el)}>Table15</Button>

                    <br />
                    <Button className="send_menu" variant="warning" color="warning" size="lg" onClick={el => this.sendMenu_to_robot(el)}> Send List Menu </Button>

                    <Button className="accept" variant="success" color="success" size="lg" onClick={values => this.accept_createtable(values)}  > Accept </Button>

                    <Button className="test" variant="danger" color="danger" size="lg" onClick={el => this.clear_list_table(el)}> Clear </Button>

                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
                    </Col>
                </Row>
             </Container>
            </div>
        );

    }

}
export default Toggle;