import React from 'react';
// import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from 'react-bootstrap/Button';
//import MaterialTableDemo from './main';
//import { makeStyles } from '@material-ui/core/styles';
import Table from 'react-bootstrap/Table';
import './main.css';
import Select from "react-dropdown-select";
import $ from 'jquery';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

// import { Route } from 'react-router-dom'
import Tables from './table';

// import Split from 'react-split'
const maxtable = 10;
const num_table = [];
var table_x_y = ['(0,0)', '(1,1)', '(1,2)', '(1,3)', '(2,1)', '(2,2)', '(2,3)', '(3,1)', '(3,2)', '(3,3)'];
var sum_have = 0;
var slot_have = [];
var table_position = 0;
var table_num = sessionStorage.getItem('table_num');
var table_row = sessionStorage.getItem('table_row');
// sessionStorage.setItem('slot_have',"img1")

class Cup extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.set_data_cup_have();

        // set el height and width etc.
    }

    change_cup(img) { //Change img cup have or not in slot

        var sets = (img.target.src).split("_");
        if (sets[1] == "dont.png") {
            img.target.src = "cups_have.png";
            //sessionStorage.setItem('img', "cups_have.png");
        }
        else {

            img.target.src = "cups_dont.png";
            //sessionStorage.setItem('img', "cups_dont.png");
        }
    }

    check_cups_have(img) { //Add cup in array list and count num of cups

        var check_src = img.target.src;

        console.log(check_src);


        var sets = sessionStorage.getItem('sets');
        sets = check_src.split("_");
        console.log("sets___" + sets);
        if (sets[1] == 'have.png') {

            if ((slot_have.find(element => element == img.target.className)) != img.target.className) { //check add cup repeat in list ?
                slot_have.push(img.target.className);
                sum_have += 1;
                console.log("add cups");



            }
        }
        else if (sets[1] == 'dont.png') {
            if ((slot_have.find(element => element == img.target.className)) == img.target.className) { // check data in list ?
                var index = slot_have.indexOf(img.target.className);
                slot_have.splice(index, 1);
                sum_have -= 1;
                console.log("del cups");


            }
        }

        sessionStorage.setItem("slot_have", slot_have);
        sessionStorage.setItem("sum_have", sum_have);
        sessionStorage.setItem("sets", sets[1]);


    }

    onClick_img(ev) {//check change cup and check cup have in list
        this.change_cup(ev)
        this.check_cups_have(ev);
    }

    set_data_cup_have() {
        if (!!sessionStorage.slot_have) {
            var cup_data = (sessionStorage.slot_have).split(",");

            if (cup_data.length > 0) {
                for (var set_cup = 0; set_cup < cup_data.length; set_cup++) {
                    var position = cup_data[set_cup].split("img")[1];
                    document.getElementById('img' + position).src = 'cups_have.png';

                }
            }
        }
    }




    render() {
        return (
            <div>

                <div className="img">
                    <img className="img1" id="img1" src="cups_dont.png" alt="Smiley face" height="200" width="180" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img2" id="img2" src="cups_dont.png" alt="Smiley face" height="200" width="180" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img3" id="img3" src="cups_dont.png" alt="Smiley face" height="200" width="180" onClick={ev => this.onClick_img(ev)}></img>
                    <br/>
                    <img className="img4" id="img4" src="cups_dont.png" alt="Smiley face" height="200" width="180" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img5" id="img5" src="cups_dont.png" alt="Smiley face" height="200" width="180" onClick={ev => this.onClick_img(ev)}></img>
                    <img className="img6" id="img6" src="cups_dont.png" alt="Smiley face" height="200" width="180" onClick={ev => this.onClick_img(ev)}></img>

                </div>

            </div>

        );

    }

}
export default Cup;