import React from 'react';
// import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from 'react-bootstrap/Button';
//import MaterialTableDemo from './main';
//import { makeStyles } from '@material-ui/core/styles';
import Table from 'react-bootstrap/Table';
import  './main.css';
import Select from "react-dropdown-select";
import $ from 'jquery'; 

// import Split from 'react-split'
const maxtable = 10;
const num_table =  [];
var table_x_y = ['(0,0)','(1,1)','(1,2)','(1,3)','(2,1)','(2,2)','(2,3)','(3,1)','(3,2)','(3,3)'];
var sum_have = 0;
var slot_have = [];
var table_position = 0;
var table_num = 0;
var table_row = 0;


class List_talble extends React.Component {
    constructor(props) {
        super(props);
        this.state = { t1: true };

        this.state = {id : 0 };
        this.state = {name : " "  };
        this.state = {menus: [

            ]
        }

    }

    get_num_table( el ){ // Show id table and position of table 
        // console.log( el.target.className );
        var split = el.target.className.split(" ")[0];
        // console.log(split);
        var num_table = split.split("tables")[1];
        console.log(num_table);
        console.log(table_x_y[num_table]);
        table_num = num_table;
        table_position = table_x_y[num_table]
        alert("ADD Table")
       
    }

    change_cup(img){ //Change img cup have or not in slot
        var sets = (img.target.src).split("_");
        if( sets[1] == "dont.png"){
            img.target.src = "cups_have.png";
        }
        else{
            img.target.src = "cups_dont.png";
        }
    }

    check_cups_have(img){ //Add cup in array list and count num of cups
        
            var check_src = img.target.src;
            
            console.log(check_src);
             var sets = check_src.split("_");
             console.log(sets);
            if( sets[1] == 'have.png' ){
                
                if( (slot_have.find(element => element == img.target.className)) != img.target.className){ //check add cup repeat in list ?
                    slot_have.push (img.target.className);
                    sum_have += 1;
                    console.log("add cups");
                    
                    
                }
            }
            else if ( sets[1] == 'dont.png' ){
                if( (slot_have.find(element => element == img.target.className)) == img.target.className){ // check data in list ?
                    var index = slot_have.indexOf(img.target.className);
                    slot_have.splice (index ,1);
                    sum_have -= 1;
                    console.log("del cups");

                }
            }
        console.log( sum_have );
        console.log( slot_have );
    }

    createTable = () => { // create table and postion array 2d
    
        for (let i = 1; i <= maxtable; i++) {
            num_table.push({ label: i, value: table_x_y[i] })
            
        }
    }

    onClick_img(ev){//check change cup and check cup have in list
        this.change_cup(ev)
        this.check_cups_have(ev);
    }
    
    accept_createtable(  ){ //accept createtable in array list
        console.log("____________________________");
        slot_have.sort();
        console.log(sum_have);
        console.log(slot_have);
        var table_last = 0;
        if( table_num != 0 ){
            for( var loop_table = 0; loop_table < slot_have.length; loop_table++){ //loop in data  array_list
                //     9 == 9 
                if( table_num != table_last){ // เช็คว่าสร้างโต๊ะมาจริงไหม ถ้าจริงทำการสร้างตารางขึ้นมา
                    document.getElementById('list_menu_history').innerHTML += "<tr id='row"+ table_row +"'></tr>";
                    document.getElementById("row"+table_row).innerHTML = "<th id='id"+ table_row +"'>"+ (table_row+1) +"</th>"
                    +"<th id='table"+ table_row +"'>"+ table_num +"</th>"
                    +"<th id='slot"+ table_row +"'>"+ slot_have[loop_table].split("img")[1] +"</th>";
                    table_last = table_num;

                }
                else{
                    document.getElementById("slot"+ table_row).innerHTML += " , "+ slot_have[loop_table].split("img")[1] +""; //list slot in table
                }
            }
            table_row++;
        }
        else{
            alert("Please Choose Table First");
        }
        
        
    }

     clear_list_table( el ) { 
        document.getElementById('list_menu_history').innerHTML = '<tr>'
        +'<th>id</th>'
        +'<th>Table</th>'
        +'<th>Slot</th>'
        +'</tr>';
        table_num = 0;
        table_row = 0;
        sum_have = 0;
        slot_have = [];
        console.log(slot_have);
        for( var num_pic = 1; num_pic <= 6; num_pic++){
            document.getElementById('img'+ num_pic).src = "cups_dont.png";
        }
     }

     sendMenu_to_robot( el ){

        var array_obj = {};
        var start_id = 0;
        if( table_row != 0){
            if( table_row != 0){
                for( start_id = 0; start_id < table_row; start_id++ ){
                    var data1 = document.getElementById('id'+ start_id).innerHTML;
                    var data2 = document.getElementById('table'+ start_id).innerHTML;
                    var data3 = document.getElementById('slot'+ start_id).innerHTML;
                    console.log("id = "+data1+" | table="+data2+" | slot="+data3);
                    var array_list = {};
                    array_list["id"] = data1;
                    array_list["table"] = data2;
                    array_list["slot"] = data3;
                    console.log("id = "+data1+" | table="+data2+" | slot="+data3);
                    array_obj[(start_id+1)] = array_list;
                }
             }
            alert("Let' Go!!")
            console.log(array_obj);
        }
        else{
            alert("Please Add List Cups Menu First")
        }
    }
    render() {       
        return (       
            <div>
            
            
            <div className = "table"  >

            <Table striped bordered hover class="table table-dark">
            <thead id="list_menu_history">
                <tr>
                    <th>id</th>
                    <th>Table</th>
                    <th>Slot</th>
                </tr>
            </thead>

                <tbody>
                </tbody>
            </Table>
            </div>


            <div className = "button" >
            
            

            <Button className = "send_menu" variant = "warning" color = "warning" size="lg"   onClick = { el => this.sendMenu_to_robot(el) }> Send List Menu </Button>
         
            <Button className = "test" variant = "danger" color = "danger" size="lg"  onClick = { el => this.clear_list_table(el) }> Clear </Button>
            
           
            </div>
           </div>   
        );
      
    }

}
export default List_talble;